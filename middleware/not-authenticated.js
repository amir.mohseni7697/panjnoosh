export default function({ store, redirect }) {
  if (store.getters['auth/isLoggedIn']) {
    redirect('/dashboard');
  }
} 
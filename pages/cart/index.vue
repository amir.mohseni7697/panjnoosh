<template>
  <div class="cart-page">
    <div class="cart">
      <div class="header">
        <h1 class="title">سبد خرید</h1>
        <div class="actions">
          <Button type="text" color='white' round size="sm" v-if="count" @click.native="onRemoveAll">
            <template #icon>
              <ClearIcon />
            </template>
          </Button>
          <Button type="text" color='white' round size="sm" @click.native="$router.push('/shop')">
            <template #icon>
              <BackIcon />
            </template>
          </Button>
        </div>
      </div>
      <div class="content">
        <div class="empty" v-if="!count">
          <EmptyIcon class="empty__icon"/>
          <p class="empty__text">سبد خرید شما خالی می باشد.</p>
        </div>
        <CartItem v-for="product in cart" :key="product.id" :product="product" />
        <div class="total" v-if="count">
          <p class="total__price">مجموع سفارش: {{price | formatPrice}} تومان</p>
        </div>
        <Address :addresses="addresses" v-if="count" :states="states" @change="onAddressChange"/>
      </div>
      <button class="cart__button" v-ripple="'rgba(0, 0, 0, .3)'" v-if="count" @click="onSubmit">ثبت سفارش</button>
    </div>
  </div>
</template>

<script>
import Button from "@/components/base/Button"
import Select from "@/components/base/Select"
import BackIcon from "@/assets/svgs/back.svg";
import AddIcon from "@/assets/svgs/add.svg";
import AddressIcon from "@/assets/svgs/location.svg";
import ClearIcon from "@/assets/svgs/remove_shopping_cart.svg";
import EmptyIcon from "@/assets/svgs/cart_outline.svg";
import { mapGetters } from "vuex";
import CartItem from "@/components/cart/CartItem";
import Address from "@/components/cart/Address";
import numeral from "numeral"

export default {
  components: {
    Button,
    BackIcon,
    ClearIcon,
    EmptyIcon,
    CartItem,
    AddressIcon,
    AddIcon,
    Select,
    Address
  },
  middleware: "authenticated",
  data() {
    return {
      address: undefined
    }
  },
  head() {
    return {
      title: "سبد خرید"
    }
  },
  computed: {
    ...mapGetters({
      count: "cart/getCount",
      cart: "cart/getCart",
      price: "cart/getPrice"
    })
  },
  async asyncData(context) {
    let states = [];
    let addresses = [];
    await context.store.dispatch('cart/updateCart');
    await context.store.dispatch('addresses/fetchAddresses', context.store.getters['auth/getToken']).then(response => {
      if (response.status === 200) {
        addresses = response.data.data;
      }
    }).catch(error => {
      console.log(error)
    });
    await context.store.dispatch('addresses/fetchStates').then(response => {
      if (response.status === 200) {
        states = response.data.data;
      }
    }).catch(error => {
      console.log(error);
    });
    return {
      addresses,
      states
    }
  },
  filters: {
    formatPrice(price) {
      return numeral(price).format('0,0');
    }
  },
  methods: {
    onRemoveAll() {
      this.$store.dispatch('cart/clear');
    },
    onAddressChange(address) {
      this.address = address;
    },
    async onSubmit(e) {
      e.preventDefault();
      if (typeof this.address === "number" && this.address >= 0) {
        this.submitOrder(this.address);
      } else if (typeof this.address === "object") {
        const data = {
          data: this.address,
          token: this.$store.getters['auth/getToken']
        };
        let address_id;
        await this.$store.dispatch('addresses/createAddress', data).then(response => {
          address_id = response.data.data.id;
        });
        this.submitOrder(address_id);
      } else {
        this.$store.dispatch('notifications/push', {
          message: 'لطفا یک آدرس انتخاب و یا یک آدرس کامل وارد کنید!',
          light: true,
          type: 'warning',
          timeout: 3000
        });
      }
    },
    submitOrder(addressId) {
      const cart = this.$store.getters['cart/getCart'];
      let products = cart.map(cur => {
        return {
          id: cur.id,
          quantity: cur.count
        }
      });
      const data = {
        user_address_id: addressId,
        products
      };
      const token = this.$store.getters['auth/getToken'];
      this.$store.dispatch('orders/createOrder', { token, data }).then(response => {
        const { id } = response.data.data.order;
        return this.$store.dispatch('orders/createTransaction', { token, id });
      }).then(response => {
        window.location.replace(response.data.data.url);
      }).catch(error => {
        if (error.response.status === 400) {
          this.$store.dispatch('notifications/push', {
            type: "error",
            message: "به اندازه ی کافی در انبار موجود نمی باشد."
          });
        }
        console.log(error);
      });
    }
  }
}
</script>

<style lang="stylus" scoped>
@import "~assets/styles/variables"

.cart-page
  padding $lg
  background-color $black
  color $white
  direction rtl
  @media only screen and (max-width 600px)
    padding $lg $md
  @media only screen and (max-width 450px)
    padding $lg $sm

.cart
  background-color $black-darken
  box-shadow $shop-container-shadow
  &__button
    outline none
    border none
    width 100%
    padding 1.8rem 0
    font-family "IRANSans"
    font-size $font-size
    background-color $primary
    &:hover
      cursor pointer
      background-color lighten($primary, 25%);
    &:focus
      background-color lighten($primary, 25%);

.title
  font-size 2.6rem
  font-weight 400
  @media only screen and (max-width 400px)
    font-size 2.3rem

.header
  display flex
  align-items center
  justify-content space-between
  padding $shop-toolbar-padding
  box-shadow $shop-toolbar-shadow
  height $shop-toolbar-height
  @media only screen and (max-width 600px)
    padding 0 1.5rem

.actions
  display flex
  align-items center
  & > *:not(:last-child)
    margin-left $sm
    @media only screen and (max-width 600px)
      margin-left $xxsm

.empty
  display flex
  flex-direction column
  align-items center
  opacity .3
  padding $xlg 0
  &__icon
    fill $white
    width 15rem
    height 15rem
    margin-bottom $sm
  &__text
    font-size 2.2rem

.total
  border-bottom 1px solid rgba(white, .1)
  padding $md
  @media only screen and (max-width 600px)
    padding $sm
  &__price
    font-size 2rem
    @media only screen and (max-width 600px)
      font-size 1.6rem
</style>
export const slides = [
  {
    id: 0,
    title: "پنج نوش",
    description: "تولید کننده انواع دمنوش های گیاهی و دارویی",
    imageUrl: "/images/pic-14.jpg",
    time: 7000
  },
  {
    id: 1,
    title: "فروشگاه سایت",
    description: "خرید راحت از فروشگاه آنلاین پنج نوش",
    imageUrl: "/images/pic-15.jpg",
    action: {
      link: "/shop",
      title: "فروشگاه"
    }
  },
  {
    id: 2,
    title: "دمنوش آرتیشو",
    description: "درمان کننده کبد چرب، محافظت سلول های کبد",
    imageUrl: "/images/artisho.jpg",
    action: {
      link: "/shop/product/11",
      title: "جزئیات بیشتر"
    },
    time: 2000
  },
  {
    id: 3,
    title: "دمنوش کاهش قند",
    description: "درمان دیابت و کنترل قند خون",
    imageUrl: "/images/pic-13.jpg",
    action: {
      link: "/shop/product/12",
      title: "جزئیات بیشتر"
    }
  }
];
import axios from "axios";

const state = () => {}

const getters = {}

const mutations = {}

const actions = {
  fetchPost(context, id) {
    return axios.get(process.env.baseUrl + `/blogs/${id}`);
  },
  fetchPosts(context) {
    return axios.get(process.env.baseUrl + '/blogs');
  },
  createPost(context, data) {
    return axios.post(process.env.baseUrl + '/blogs', data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  deletePost(context, data) {
    return axios.delete(process.env.baseUrl + `/blogs/${data.id}`, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  updatePost(context, data) {
    return axios.patch(process.env.baseUrl + `/blogs/${data.id}`, data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  }
}

export default {state, mutations, getters, actions}
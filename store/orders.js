import axios from "axios";

const state = () => {}

const getters = {}

const mutations = {}

const actions = {
  fetchUserOrders(context, data) {
    return axios.get(process.env.baseUrl + `/orders`, {
      headers: {
        "Authorization": `Bearer ${data.token}`
      },
      params: {
        page: data.page || 1
      }
    });
  },
  fetchOrder(contex, data) {
    return axios.get(process.env.baseUrl + `/orders/${data.id}`, {
      headers: {
        "Authorization": `Bearer ${data.token}`
      }
    });
  },
  createOrder(context, data) {
    return axios.post(process.env.baseUrl + `/orders`, data.data, {
      headers: {
        "Authorization": `Bearer ${data.token}`
      }
    });
  },
  createTransaction(context, data) {
    return axios.post(process.env.baseUrl + `/transactions`, {order_id: data.id}, {
      headers: {
        "Authorization": `Bearer ${data.token}`
      }
    });
  },
  fetchOrders(context, data) {
    return axios.get(process.env.baseUrl + `/admins/orders`, {
      headers: {
        "Authorization": `Bearer ${data.token}`
      },
      params: {
        page: data.page || 1
      }
    });
  },
  updateStatus(context, data) {
    return axios.patch(process.env.baseUrl + `/admins/orders/${data.id}`, {
      status: data.status
    }, {
      headers: {
        "Authorization": `Bearer ${data.token}`
      }
    });
  },
  verifyPayment(context, data) {
    axios.get(process.env.baseUrl + `/transactions/verify`, {
      params: {
        "Authority": data.authority,
        "Status": data.status
      }
    });
  }
}

export default {state, mutations, getters, actions}
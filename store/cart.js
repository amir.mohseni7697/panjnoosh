import cookie from "js-cookie";

const state = () => {
  return {
    cart: []
  }
}

const getters = {
  getCount: state => {
    let count = 0;
    state.cart.forEach(cur => {
      count += cur.count;
    });
    return count;
  },
  getCart: state => state.cart,
  getPrice: state => {
    let sum = 0;
    state.cart.forEach(cur => sum += cur.price * (100 - cur.off) / 100 * cur.count)
    return sum;
  }
}

const mutations = {
  plus: (state, product) => {
    const count = state.cart.find(cur => cur.id === product.id).count + 1;
    const index = state.cart.findIndex(cur => cur.id === product.id);
    state.cart = state.cart.filter(cur => cur.id !== product.id);
    product.count = count;
    state.cart.splice(index, 0, product);
  },
  minus: (state, id) => {
    state.cart.find(cur => cur.id === id).count--;
  },
  push: (state, product) => {
    product.count = 1;
    state.cart.push(product);
  },
  remove: (state, id) => {
    state.cart = state.cart.filter(cur => cur.id !== id);
  },
  updateProduct: (state, product) => {
    const count = state.cart.find(cur => cur.id === product.id).count;
    const index = state.cart.findIndex(cur => cur.id === product.id);
    state.cart = state.cart.filter(cur => cur.id !== product.id);
    product.count = count;
    state.cart.splice(index, 0, product);
  },
  clear: (state) => (state.cart = []),
  setCart: (state, cart) => (state.cart = cart)
}

const actions = {
  push: ({ commit, state, dispatch, rootGetters }, id) => {
    return new Promise(async (resolve, reject) => {
      if (!rootGetters['auth/isLoggedIn']) {
        reject('auth');
      };
      let product = {};
      await dispatch('shop/fetchProduct', id, {root: true}).then(response => {
        product = response.data.data;
      }).catch(error => {
        reject(error);
      });
      const result = state.cart.find(cur => cur.id === id);
      if (result) {
        if (result.count + 1 <= product.quantity) {
          commit('plus', product);
          dispatch('setCookie');
          resolve('done');
        } else {
          reject('quantity');
        }
      } else {
        if (product.quantity > 0) {
          commit('push', product);
          dispatch('setCookie');
          resolve('done');
        } else {
          reject('quantity');
        }
      }
    });
  },
  pop: ({ commit, state, dispatch }, id) => {
    if (state.cart.find(cur => cur.id === id)) {
      commit('minus', id);
      if (state.cart.find(cur => cur.id === id).count === 0) {
        commit('remove', id);
      }
    }
    dispatch('setCookie');
  },
  remove: ({ commit, state, dispatch }, id) => {
    if (state.cart.find(cur => cur.id === id)) {
      commit('remove', id);
      dispatch('setCookie');
    }
  },
  clear: ({ commit, dispatch }) => {
    commit('clear');
    dispatch('setCookie');
  },
  updateCart: async ({ state, dispatch, commit }) => {
    return new Promise(async (resolve, reject) => {
      for (let i = 0; i < state.cart.length; i++) {
        await dispatch('shop/fetchProduct', state.cart[i].id, {root: true}).then(response => {
          let product = response.data.data;
          product.count = state.cart[i].count;
          commit('updateProduct', product);
        }).catch(error => {
          if (error.response.status === 404) {
            dispatch('clear');
          }
          // reject(error);
        });
      }
      resolve('done');
    });
  },
  setCookie: ({ state }) => {
    let cart = state.cart.map(cur => {
      return {
        id: cur.id,
        count: cur.count
      }
    });
    cookie.set("cart", cart);
  }
}

export default { state, getters, mutations, actions }
import cookie from 'cookie';

const actions = {
  nuxtServerInit: async ({ commit, dispatch }, { req }) => {
    if (req.headers.cookie) {
      const token = cookie.parse(req.headers.cookie).token;
      const userId = cookie.parse(req.headers.cookie).userId;
      const cart = cookie.parse(req.headers.cookie).cart;
      if (token && userId) {
        commit('auth/setToken', token);
        commit('auth/setUserId', userId);
        await dispatch('auth/fetchMe', token).catch(error => {
          if (error.status === 401) {
            commit('auth/setToken', '');
            commit('auth/setUserId', '');
          }
        });
        if (cart) {
          commit('cart/setCart', JSON.parse(cart));
        }
      } else {
        commit('auth/setToken', "");
        commit('auth/setUserId', "");
      }
    }
  }
}

export default { actions }
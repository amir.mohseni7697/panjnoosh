import axios from "axios";

const state = () => {
  return {
    viewStyle: "grid",
    isCategoryNavShown: false
  }
};

const getters = {
  getViewStyle: state => state.viewStyle,
  isCategoryNavShown: state => state.isCategoryNavShown,
  getBackRoute: state => state.backRoute
};

const actions = {
  setViewStyleGrid: ({ commit }) => {
    commit("setViewStyle", "grid");
  },
  setViewStyleList: ({ commit }) => {
    commit("setViewStyle", "list");
  },
  toggleIsCategoryNavShown: ({ commit, getters }) => {
    commit("setIsCategoryNavShown", !getters.isCategoryNavShown);
  },
  fetchCategories: () => {
    return axios.get(process.env.baseUrl + `/categories`);
  },
  fetchCategory: (context, id) => {
    return axios.get(process.env.baseUrl + `/categories/${id}`);
  },
  fetchCategoryProducts: (context, id) => {
    return axios.get(process.env.baseUrl + `/categories/${id}/products`);
  },
  fetchProducts: () => {
    return axios.get(process.env.baseUrl + `/products`);
  },
  fetchSearchResult: (context, search) => {
    return axios.get(process.env.baseUrl + `/products?search=${search}`);
  },
  fetchProduct: (context, id) => {
    return axios.get(process.env.baseUrl + `/products/${id}`);
  },
  createCategory: (context, data) => {
    return axios.post(process.env.baseUrl + `/categories`, data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  deleteCategory: (context, data) => {
    return axios.delete(process.env.baseUrl + `/categories/${data.id}`, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  updateCategory: (context, data) => {
    return axios.patch(process.env.baseUrl + `/categories/${data.id}`, data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  deleteProduct: (context, data) => {
    return axios.delete(process.env.baseUrl + `/products/${data.id}`, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  createProduct: (context, data) => {
    return axios.post(process.env.baseUrl + `/products`, data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  updateProduct: (context, data) => {
    return axios.patch(process.env.baseUrl + `/products/${data.id}`, data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  }
};

const mutations = {
  setViewStyle: (state, val) => (state.viewStyle = val),
  setIsCategoryNavShown: (state, val) => (state.isCategoryNavShown = val)
};

export default { state, getters, actions, mutations };

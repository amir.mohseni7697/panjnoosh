import uuidv1 from "uuid/v1";

const state = () => {
  return {
    notifications: []
  }
}

const getters = {
  getNotifications: state => state.notifications
}

const mutations = {
  pushNotification: (state, notification) => {
    state.notifications.push(notification);
  },
  setNotifications: (state, list) => {
    state.notifications = list;
  },
  removeNotification: (state, id) => {
    state.notifications = state.notifications.filter(notification => notification.id !== id);
  },
  incrementStatus: (state, id) => {
    state.notifications.find(notification => notification.id === id).status++;
  }
}

const actions = {
  push: ({ commit }, { message='', type='warning', light=false, timeout=2500, speed=320 }) => {
    const id  = uuidv1();
    commit('pushNotification', { id, message, type, light, status: 0, speed});
    setTimeout(() => {
      commit('incrementStatus', id);
    }, speed)

    setTimeout(() => {
      commit('incrementStatus', id);
    }, speed + timeout)

    setTimeout(() => {
      commit('removeNotification', id);
    }, speed + timeout + speed) 
  },
}


export default { state, actions, getters, mutations}
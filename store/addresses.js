import axios from "axios";

const state = () => {}

const getters = {}

const mutations = {}

const actions = {
  fetchAddresses: (context, token) => {
    return axios.get(process.env.baseUrl + '/users/addresses', {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });
  },
  fetchAddress: (context, data) => {
    return axios.get(process.env.baseUrl + `/users/addresses/${data.id}`, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  fetchStates: (context) => {
    return axios.get(process.env.baseUrl + '/states');
  },
  fetchState: (context, id) => {
    return axios.get(process.env.baseUrl + `/states/${id}`);
  },
  createAddress: (context, info) => {
    return axios.post(process.env.baseUrl + '/users/addresses', info.data, {
      headers: {
        'Authorization': `Bearer ${info.token}`
      }
    });
  },
  updateAddress: (context, info) => {
    return axios.patch(process.env.baseUrl + `/users/addresses/${info.id}`, info.data, {
      headers: {
        'Authorization': `Bearer ${info.token}`
      }
    });
  },
  deleteAddress: (context, info) => {
    return axios.delete(process.env.baseUrl + `/users/addresses/${info.id}`, {
      headers: {
        'Authorization': `Bearer ${info.token}`
      }
    });
  }
}

export default {state, mutations, getters, actions}
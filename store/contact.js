import axios from "axios";

const actions = {
  sendEmail(context, data) {
    return axios.post(process.env.baseUrl + `/email`, data.data);
  }
}

export default { actions };
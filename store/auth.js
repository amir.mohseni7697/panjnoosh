import axios from "axios";
import cookie from "js-cookie";

const state = () => {
  return {
    token: "", // should read from cookies
    userId: "", // should read from cookies
    user: {}
  }
}

const getters = {
  isLoggedIn: state => {
    return state.token ? true : false;
  },
  isAdmin: state => {
    return state.user.role === 'admin' ? true: false;
  },
  getToken: state => state.token,
  getUserId: state => state.userId,
  getUser: state => state.user
}


const mutations = {
  setToken: (state, token) => {
    state.token = token;
  },
  setUserId: (state, userId) => {
    state.userId = userId;
  },
  setUser: (state, user) => {
    state.user = user;
  }
}

const actions = {
  login: ({ commit, dispatch }, {username, password}) => {
    // 1. send the request
    return new Promise((resolve, reject) => {
      axios.post(process.env.baseUrl + '/auth/signin', {
        username: username,
        password: password
      }).then(async response => {
        if (response.status === 200) {
          // 2. commit the token and userId
          commit('setToken', response.data.data.token);
          cookie.set("token", response.data.data.token);
          commit('setUserId', response.data.data.id);
          cookie.set("userId", response.data.data.id);
          await dispatch('fetchMe', response.data.data.token);
          resolve(response);
        }
        reject(response)
      }).catch(error => {
        reject(error)
      });
    });
    // 3. set cookies
    // 4. return a promise
  },
  logout: ({ commit }) => {
    // 1. clear token and userId from store
    commit('setToken', "");
    commit('setUserId', "");
    commit('setUser', {});
    // 2. clear token and userId from cookies
    cookie.remove('token');
    cookie.remove('userId');
    // 3. return a promise
    return new Promise((resolve, reject) => {
      resolve();
    });
  },
  register: ({ commit, dispatch }, {username, password, passwordConfirmation}) => {
    // 1. send the request 
    return new Promise((resolve, reject) => {
      axios.post(process.env.baseUrl + '/auth/signup', {
        username: username,
        password: password,
        password_confirmation: passwordConfirmation
      }).then(async response => {
        if (response.status === 201) {
          // 2. commit the token and userId     
          // 3. set cookies
          commit('setToken', response.data.data.token);
          cookie.set("token", response.data.data.token);
          commit('setUserId', response.data.data.id);
          cookie.set("userId", response.data.data.id);
          await dispatch('fetchMe', response.data.data.token);
          resolve(response);
        }
        reject(response)
      }).catch(error => {
        reject(error)
      });
    });
    // 4. return a promise
  },
  fetchMe: (context, token) => {
    return new Promise((resolve, reject) => {
      axios.get(process.env.baseUrl + '/users/me', {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }).then(response => {
        if (response.status === 200) {
          context.commit('setUser', response.data.data);
          resolve(response);
        }
        reject(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  updateUserInfo: (context, data) => {
    return new Promise(async (resolve, reject) => {
      await axios.patch(process.env.baseUrl + '/users/me', data.info, {
        headers: {
          'Authorization': `Bearer ${data.token}`
        }
      }).then(response => {
        if (response.status === 200) {
          context.commit('setUser', response.data.data);
          resolve(response);
        }
        reject(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  updateAvatar: (context, data) => {
    const formData = new FormData();
    formData.append('profile', data.avatar);
    return new Promise(async (resolve, reject) => {
      await axios.post(process.env.baseUrl + '/users/me/profile', formData, {
        headers: {
          'Authorization': `Bearer ${data.token}`,
          "Content-Type": "multipart/form-data"
        }
      }).then(response => {
        if (response.status === 200) {
          context.dispatch('fetchMe', context.getters['getToken']);
          resolve(response);
        }
        reject(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  fetchUsers: (context, data) => {
    return axios.get(process.env.baseUrl + '/users', {
      headers: {
        'Authorization': `Bearer ${data.token}`
      },
      params: {
        page: data.page || 1
      }
    });
  },
  deleteUser: (context, data) => {
    return axios.delete(process.env.baseUrl + `/users/${data.id}`, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  fetchUser: (context, data) => {
    return axios.get(process.env.baseUrl + `/users/${data.id}`, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  updateUserRole: (context, data) => {
    return axios.patch(process.env.baseUrl + `/users/${data.id}`, {
      role: data.role
    }, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  },
  createUser: (context, data) => {
    return axios.post(process.env.baseUrl + `/users`, data.data, {
      headers: {
        'Authorization': `Bearer ${data.token}`
      }
    });
  }
}

export default { state, getters, mutations, actions }
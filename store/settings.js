const state = () => {
  return {
    navigationLinks: [
      {
        title: 'فروشگاه',
        route: '/shop'
      },
      // {
      //   title: 'پیشنهادات ویژه',
      //   route: '/#special-offers'
      // }
      // ,
      {
        title: 'بلاگ',
        route: '/blog'
      },
      {
        title: 'درباره ما',
        route: '/about'
      },
      {
        title: 'ارتباط با ما',
        route: '/contact'
      }
    ],
    backRoute: "",
    dashboardPageTitle: ""
  }
}

const getters = {
  getNavigationLinks: state => state.navigationLinks,
  getBackRoute: state => state.backRoute,
  getDashboardPageTitle: state => state.dashboardPageTitle
}

const mutations = {
  setNavigationLinks: (state, links) => (state.navigationLinks = links),
  setBackRoute: (state, val) => (state.backRoute = val),
  setDashboardPageTitle: (state, val) => (state.dashboardPageTitle = val)
}

const actions = {
  addNavigationLink: ({ commit, state }, link) => {
    let list = state.navigationLinks.map(cur => cur);
    list.push(link);
    commit('setNavigationLinks', list);
  },
  setBackRoute: ({ commit }, val) => {
    commit("setBackRoute", val);
  },
  setDashboardPageTitle: ({ commit }, val) => {
    commit('setDashboardPageTitle', val);
  }
}

export default {state, mutations, getters, actions}